package main

import "gitlab.com/kevinsumner/wanderer/cmd"

func main() {
	cmd.Execute()
}
