package wander

import (
	"fmt"
	"time"

	log "github.com/sirupsen/logrus"
	"github.com/spf13/viper"
	"gitlab.com/kevinsumner/wanderer/dnsproviders"
	"gitlab.com/kevinsumner/wanderer/ipproviders"
	"gitlab.com/kevinsumner/wanderer/util"
)

type host_config struct {
	Zone        string
	Hostname    string
	IPProvider  string `mapstructure:"ip_provider"`
	DNSProvider string `mapstructure:"dns_provider"`
	EnableIPv4  bool   `mapstructure:"ipv4"`
	EnableIPv6  bool   `mapstructure:"ipv6"`
}

func Wander() error {
	if viper.GetBool("debug") {
		log.SetLevel(log.DebugLevel)
	} else {
		log.SetLevel(log.WarnLevel)
	}

	interval := time.Duration(time.Duration(viper.GetInt("interval_minutes")) * time.Minute)
	if interval == 0 {
		log.Info("Interval set to 0, running only once")
		viper.Set("run-once", true)
	}

	var hosts []host_config
	viper.UnmarshalKey("host", &hosts)

	// main loop
	for true {
		for _, host := range hosts {
			hostname := host.Hostname
			zone := host.Zone
			ipprovider := host.IPProvider
			dnsprovider := host.DNSProvider
			ipv4 := host.EnableIPv4
			ipv6 := host.EnableIPv6

			if !ipv4 && !ipv6 {
				log.Warningf("IPv4 and IPv6 updates disabled for %s; skipping.", hostname)
			}

			if ipv4 {
				log.Infof("Checking for IPv4 update for %s", hostname)
				if err := checkAndUpdate(hostname, zone, util.AF_INET, ipprovider, dnsprovider); err != nil {
					fmt.Printf("Error performing IPv4 check/update: %s\n", err)
				}
			}

			if ipv6 {
				log.Infof("Checking for IPv6 update for %s", hostname)
				if err := checkAndUpdate(hostname, zone, util.AF_INET6, ipprovider, dnsprovider); err != nil {
					fmt.Printf("Error performing IPv6 check/update: %s\n", err)
				}
			}
		}

		if viper.GetBool("run-once") {
			break
		}

		log.Debugf("Sleeping for %s", interval)
		time.Sleep(interval)
	}
	log.Info("Done")

	return nil
}

func checkAndUpdate(hostname string, zone string, af util.AddressFamily, ipprovider string, dnsprovider string) (err error) {
	// Get external IP
	var ipp ipproviders.IPProvider
	ipp, err = ipproviders.GetProvider(ipprovider)
	if err != nil {
		return fmt.Errorf("Couldn't get IP provider for %s: %v", hostname, err)
	}

	var dnsp dnsproviders.DNSProvider
	dnsp, err = dnsproviders.GetProvider(dnsprovider)
	if err != nil {
		return fmt.Errorf("Couldn't get DNS provider for %s: %v", hostname, err)
	}

	extIP, err := ipp.GetExternalAddress(af)
	if err != nil {
		return fmt.Errorf("Couldn't get external IP for %s: %v", hostname, err)
	}
	log.Infof("External IP is %s", extIP)

	// Get DNS record
	dnsIP, err := dnsp.GetAddress(hostname, zone, af)
	if err != nil {
		return fmt.Errorf("Couldn't get DNS record for %s: ", hostname, err)
	}
	log.Infof("DNS has IP %s", dnsIP)

	if extIP.Equal(dnsIP) {
		log.Infof("External IP and IP in DNS record for %s match; no update needed", hostname)
		return nil
	}

	log.Warningf("External IP (%s) and IP in DNS record (%s) don't match; updating", extIP, dnsIP)

	// Update DNS record
	if err = dnsp.UpdateRecord(hostname, zone, extIP, af); err != nil {
		return fmt.Errorf("Error updating DNS record '%s': %v", hostname, err)
	}

	log.Warningf("Updated %s in DNS to %s", hostname, extIP)

	return nil
}
