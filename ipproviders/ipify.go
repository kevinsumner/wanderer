package ipproviders

import (
	"fmt"
	"io/ioutil"
	"net"
	"net/http"

	log "github.com/sirupsen/logrus"
	"gitlab.com/kevinsumner/wanderer/util"
)

type Ipify struct{}

var url = map[util.AddressFamily]string{
	util.AF_INET:  "https://api.ipify.org?format=text",
	util.AF_INET6: "https://api6.ipify.org?format=text",
}

func (ipify Ipify) GetExternalAddress(af util.AddressFamily) (net.IP, error) {
	url := url[af]

	log.Debugf("Getting IP address from Ipify")
	resp, err := http.Get(url)
	if err != nil {
		return nil, fmt.Errorf("Error getting external IPv4 address from ipify: %v", err)
	}
	defer resp.Body.Close()
	ip, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, fmt.Errorf("Error reading response for IPv4 address from ipify: %v", err)
	}

	return net.ParseIP(string(ip)), nil
}
