package ipproviders

import (
	"fmt"
	"net"

	"gitlab.com/kevinsumner/wanderer/util"
)

type IPProvider interface {
	GetExternalAddress(util.AddressFamily) (net.IP, error)
}

func GetProvider(provider string) (IPProvider, error) {
	if provider == "cloudflare" {
		return Cloudflare{}, nil
	}
	if provider == "default_ip" {
		return DefaultIP{}, nil
	}
	if provider == "ipify" {
		return Ipify{}, nil
	}
	return nil, fmt.Errorf("Unknown IP provider type given: %s", provider)
}
