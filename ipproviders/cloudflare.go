package ipproviders

import (
	"bufio"
	"context"
	"fmt"
	"io/ioutil"
	"net"
	"net/http"
	"strings"
	"time"

	log "github.com/sirupsen/logrus"
	"gitlab.com/kevinsumner/wanderer/util"
)

type Cloudflare struct{}

var dialNetwork = map[util.AddressFamily]string{
	util.AF_INET:  "tcp4",
	util.AF_INET6: "tcp6",
}

func (cloudflare Cloudflare) GetExternalAddress(af util.AddressFamily) (net.IP, error) {
	const url = "https://cloudflare.com/cdn-cgi/trace"

	dialer := &net.Dialer{
		Timeout:   10 * time.Second,
		KeepAlive: 10 * time.Second,
	}

	transport := &http.Transport{
		Proxy:             http.ProxyFromEnvironment,
		ForceAttemptHTTP2: false,
		DialContext: func(ctx context.Context, network string, addr string) (net.Conn, error) {
			return dialer.DialContext(ctx, dialNetwork[af], addr)
		},
	}

	httpClient := &http.Client{
		Transport: transport,
		Timeout:   10 * time.Second,
	}

	log.Debugf("Getting IP address from Cloudflare")
	resp, err := httpClient.Get(url)
	if err != nil {
		return nil, fmt.Errorf("Error getting external IP address from Cloudflare: %v", err)
	}
	defer resp.Body.Close()
	body := resp.Body

	var ip string = ""
	scanner := bufio.NewScanner(body)
	for scanner.Scan() {
		parts := strings.SplitN(scanner.Text(), "=", 2)
		if parts[0] == "ip" {
			ip = parts[1]
			break
		}
	}

	if ip == "" {
		body_str, err := ioutil.ReadAll(body)
		if err != nil {
			return nil, fmt.Errorf("Response from Cloudflare unreadable: %v", err)
		}
		return nil, fmt.Errorf("Response from Cloudflare contained no 'ip=' lines; body:\n%s", body_str)
	}

	return net.ParseIP(string(ip)), nil
}
