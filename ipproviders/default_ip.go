package ipproviders

import (
	"net"

	log "github.com/sirupsen/logrus"
	"gitlab.com/kevinsumner/wanderer/util"
)

type DefaultIP struct{}

func (defaultip DefaultIP) GetExternalAddress(af util.AddressFamily) (net.IP, error) {
	var dialNetwork = map[util.AddressFamily]string{
		util.AF_INET:  "udp4",
		util.AF_INET6: "udp6",
	}[af]

	// I don't love these hard-codes, but it's necessary to use some,
	// always-off-network IPs.
	var dialAddr = map[util.AddressFamily]string{
		util.AF_INET:  "1.1.1.1:1",
		util.AF_INET6: "[2606:4700:4700::1111]:1",
	}[af]

	conn, err := net.Dial(dialNetwork, dialAddr)
	if err != nil {
		log.Fatal(err)
	}
	defer conn.Close()

	ip := conn.LocalAddr().(*net.UDPAddr).IP

	return ip, nil
}
