# wanderer

Wanderer is a service to detect external IP changes, e.g., due to dynamic DHCP
leases and update a DNS provider as needed.

At the moment, Wanderer:
- is IPv4-only
- can only use [ipify](https://ipify.org) for external IP discovery
- can only check and update [Cloudflare](https://cloudflare.org) DNS.

## License

Apache License 2.0.  See [LICENSE](LICENSE).
