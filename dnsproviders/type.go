package dnsproviders

import (
	"fmt"
	"net"

	"gitlab.com/kevinsumner/wanderer/util"
)

type DNSProvider interface {
	GetAddress(string, string, util.AddressFamily) (net.IP, error)
	UpdateRecord(string, string, net.IP, util.AddressFamily) error
}

func GetProvider(provider string) (DNSProvider, error) {
	if provider == "cloudflare" {
		return Cloudflare{}, nil
	}
	return nil, fmt.Errorf("Unknown DNS provider type given: %s", provider)
}
