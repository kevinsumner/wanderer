package dnsproviders

import (
	"fmt"
	"net"

	"github.com/cloudflare/cloudflare-go"
	log "github.com/sirupsen/logrus"
	"github.com/spf13/viper"
	"gitlab.com/kevinsumner/wanderer/util"
)

type Cloudflare struct{}

// Gets a fully-initialized representation of the Cloudflare API
func (cf Cloudflare) getAPIObject() (*cloudflare.API, error) {
	token := viper.GetString("dns.cloudflare.api_token")

	cfapi, err := cloudflare.NewWithAPIToken(token)
	if err != nil {
		return nil, fmt.Errorf("Problem setting up Cloudflare API: %v", err)
	}

	return cfapi, nil
}

// Gets DNS records from Cloudflare
func (cf Cloudflare) getRecords(hostname string, zone string, rectype string, cfapi *cloudflare.API) ([]cloudflare.DNSRecord, error) {
	zoneID, err := cfapi.ZoneIDByName(zone)
	if err != nil {
		return nil, fmt.Errorf("Error retrieving zone %s from Cloudflare: %v", zone, err)
	}
	log.Debugf("Zone id for %s is %s\n", zone, zoneID)

	log.Debugf("Querying Cloudflare for %s\n", hostname)
	recordQuery := cloudflare.DNSRecord{Name: hostname, Type: rectype}
	records, err := cfapi.DNSRecords(zoneID, recordQuery)
	if err != nil {
		return nil, fmt.Errorf("Error retrieving hostname %s from Cloudflare: %v", hostname, err)
	}

	return records, nil
}

// Given a hostname, pulls the DNS record and returns a single IP address.
// Returns error if >1 record in DNS.
func (cf Cloudflare) GetAddress(hostname string, zone string, af util.AddressFamily) (net.IP, error) {
	cfapi, err := cf.getAPIObject()
	if err != nil {
		return nil, fmt.Errorf("Setting up Cloudflare API failed: %v", err)
	}

	records, err := cf.getRecords(hostname, zone, util.AFToRecType(af), cfapi)
	if err != nil {
		return nil, fmt.Errorf("Error getting records from Cloudflare: %v", err)
	}

	if len(records) != 1 {
		return nil, fmt.Errorf("Expected 1 record from Cloudflare for %s, got %d", hostname, 0)
	}

	for _, r := range records {
		log.Debugf("Cloudflare has record %s %s %s\n", r.Name, r.Type, r.Content)
	}

	return net.ParseIP(records[0].Content), nil
}

func (cf Cloudflare) UpdateRecord(hostname string, zone string, newIP net.IP, af util.AddressFamily) error {
	cfapi, err := cf.getAPIObject()
	if err != nil {
		return fmt.Errorf("Setting up Cloudflare API failed: %v", err)
	}

	zoneID, err := cfapi.ZoneIDByName(zone)
	if err != nil {
		return err
	}
	log.Debugf("Zone id for %s is %s\n", zone, zoneID)

	recordQuery := cloudflare.DNSRecord{Name: hostname, Type: util.AFToRecType(af)}
	records, err := cfapi.DNSRecords(zoneID, recordQuery)
	if err != nil {
		return fmt.Errorf("Error retrieving %s records for %s from Cloudflare: %v", recordQuery.Type, recordQuery.Type, err)
	}

	if len(records) != 1 {
		return fmt.Errorf("Expected 1 record from Cloudflare for %s, got %d", hostname, 0)
	}

	recordID := records[0].ID
	newDNSRecord := cloudflare.DNSRecord{Name: hostname, Type: util.AFToRecType(af), Content: newIP.String()}

	err = cfapi.UpdateDNSRecord(zoneID, recordID, newDNSRecord)
	if err != nil {
		return fmt.Errorf("Error updating records in Cloudflare: %v", err)
	}

	return nil
}
