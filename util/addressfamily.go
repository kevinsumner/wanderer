package util

type AddressFamily uint

const (
	AF_INET  AddressFamily = iota
	AF_INET6 AddressFamily = iota
)

var afToRecType = map[AddressFamily]string{
	AF_INET:  "A",
	AF_INET6: "AAAA",
}

// Returns a forward-lookup DNS record type relevant to the address family
func AFToRecType(af AddressFamily) string {
	return afToRecType[af]
}
