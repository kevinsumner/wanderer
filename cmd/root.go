package cmd

import (
	"fmt"
	"os"
	"path"

	homedir "github.com/mitchellh/go-homedir"
	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/kevinsumner/wanderer/wander"
)

var cfgFile string

var rootCmd = &cobra.Command{
	Use:   "wanderer",
	Short: "Keeps changing IPs updated in DNS",
	Run: func(cmd *cobra.Command, args []string) {
		if err := wander.Wander(); err != nil {
			log.Panicf("wanderer failed: %v", err)
		}
	},
}

func Execute() {
	if err := rootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}

func init() {
	cobra.OnInitialize(initConfig)
	rootCmd.PersistentFlags().StringVar(&cfgFile, "config", "", "config file (default is $HOME/.config/wanderer.conf)")
	rootCmd.Flags().BoolP("debug", "D", false, "Run in debug mode")
	viper.BindPFlag("debug", rootCmd.Flags().Lookup("debug"))
}

func initConfig() {
	viper.SetConfigType("toml")
	viper.SetEnvPrefix("WANDERER_")

	if cfgFile != "" {
		viper.SetConfigFile(cfgFile)
	} else {
		home, err := homedir.Dir()
		if err != nil {
			fmt.Println(err)
			os.Exit(1)
		}

		viper.AddConfigPath("/etc")
		viper.AddConfigPath(path.Join(home, ".config"))
		viper.SetConfigName("wanderer")
	}

	viper.AutomaticEnv()

	if err := viper.ReadInConfig(); err != nil {
		panic(fmt.Sprintf("Reading config failed: %v", err))
	}
	fmt.Println("Using config file:", viper.ConfigFileUsed())
}
